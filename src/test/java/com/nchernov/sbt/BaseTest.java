package com.nchernov.sbt;

import com.nchernov.sbt.entity.Operation;
import com.nchernov.sbt.utils.OperationParser;
import junit.framework.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * Created by zugzug on 22.09.15.
 */
public class BaseTest {

    @Parameter
    BigDecimal primaryOperand;
    @Parameter
    BigDecimal secondaryOperand;
    @Parameter
    char operationSymbol;

    @Step
    public static final boolean checkCorrectnes(Operation operation) {
        return operation.evaluate().equals(operation.getResult());
    }

    @Step
    public List<Operation> parseForOperations(InputStream stream) throws IOException, ParseException {
        return OperationParser.parseForData(stream);
    }

    public void testTemplate(String filePath) throws ParseException {
        testTemplate(filePath, true);
    }

    public void testTemplate(String filePath, boolean expectCorrectness) throws ParseException {
        try {
            List<Operation> operations = parseForOperations(this.getClass().getClassLoader().getResourceAsStream(filePath));
            for(Operation operation: operations) {
                primaryOperand = operation.getPrimaryOperand();
                secondaryOperand = operation.getSecondaryOperand();
                operationSymbol = operation.getOperation();
                boolean isCorrect = checkCorrectnes(operation);
                if (expectCorrectness) {
                    Assert.assertTrue("Incorrect operation: " + operation, isCorrect);
                } else {
                    Assert.assertTrue("Incorrect operation: " + operation, !isCorrect);
                }

            }
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void singleRowIsCorrect() throws ParseException {
        testTemplate("data_single.csv");
    }

    @Test
    public void bigNumbers() throws ParseException {
        testTemplate("data_big_numbers.csv");
    }

    @Test
    public void operationsAreCorrect() throws ParseException {
        testTemplate("data.csv");
    }

    @Test
    public void operationsIsIncorrect() throws ParseException {
        testTemplate("data_wrong.csv", false);
    }

    @Test
    public void emptyRows() throws ParseException {
        testTemplate("data_empty.csv", false);
    }

    @Test(expected = ParseException.class)
    public void corruptedRows() throws ParseException {
        testTemplate("data_corrupted.csv", false);
    }

    @Test(expected = ParseException.class)
    public void corruptedOperationChar() throws ParseException {
        testTemplate("data_corrupted_char.csv", true);
    }

}
