package com.nchernov.sbt.entity;

import java.math.BigDecimal;

/**
 * Created by zugzug on 22.09.15.
 * Represents single operation row of regular format
 * @ThreadSafe - Immutable class
 */
public class Operation {
    private final char operation;
    private final BigDecimal primaryOperand;
    private final BigDecimal secondaryOperand;
    private final BigDecimal result;

    public Operation(BigDecimal primaryOperand, BigDecimal secondaryOperand, char operation, BigDecimal result) {
        this.primaryOperand = new BigDecimal(primaryOperand.toString());
        this.secondaryOperand = new BigDecimal(secondaryOperand.toString());
        this.operation = operation;
        this.result = new BigDecimal(result.toString());
    }

    public char getOperation() {
        return operation;
    }

    public BigDecimal getPrimaryOperand() {
        return primaryOperand;
    }

    public BigDecimal getSecondaryOperand() {
        return secondaryOperand;
    }

    public BigDecimal getResult() {
        return result;
    }

    public BigDecimal evaluate() {
        switch (operation) {
            case '+':
                return primaryOperand.add(secondaryOperand);
            case '-':
                return primaryOperand.subtract(secondaryOperand);
            case '*':
                return primaryOperand.multiply(secondaryOperand);
            case '/':
                return primaryOperand.divide(secondaryOperand);
            case '%':
                return primaryOperand.remainder(secondaryOperand);
            default:
            throw new UnsupportedOperationException();
        }
    }

    public String toString() {
        return String.format("{%s %s %s = %s}", primaryOperand, operation, secondaryOperand, result);
    }
}
