package com.nchernov.sbt.utils;

import com.nchernov.sbt.entity.Operation;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zugzug on 22.09.15.
 */
public class OperationParser {
    private static final String DELIMITER_DEFAULT = ";";
    private interface ParsingStrategy {
        public Operation parse(String[] row) throws ParseException;
        public static final ParsingStrategy POLAND_RECORD = new ParsingStrategy() {
            @Override
            public Operation parse(String[] row) throws ParseException {
                try {
                    char operation;
                    try {
                       operation = row[2].charAt(0);
                    } catch (StringIndexOutOfBoundsException ex) {
                        throw new ParseException("Cannot parse operation symbol", 2);
                    }
                    return new Operation(new BigDecimal(row[0]), new BigDecimal(row[1]),
                            operation,
                            new BigDecimal(row[3]));
                } catch (NumberFormatException ex) {
                    throw new ParseException("Cannot parse operand|result", -1);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    throw new ParseException("Row is not final. Broken structure.", -1);
                } catch (Exception ex) {
                    throw new ParseException("Cannot parse operation row: " + ex.getMessage(), -1);
                }

            }
        };
    };

    public static List<Operation> parseForData(InputStream stream) throws IOException, ParseException {
        return parseForData(stream, null);
    }

    public static List<Operation> parseForData(InputStream stream, String delimiter) throws FileNotFoundException, IOException, ParseException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line = null;
        int counter = 0;
        List<Operation> operations = new ArrayList<Operation>();
        while ((line = bufferedReader.readLine()) != null) {
            try {
                operations.add(ParsingStrategy.POLAND_RECORD.parse(line.split(StringUtils.isEmpty(delimiter) ? DELIMITER_DEFAULT : delimiter, -1)));
            } catch (ParseException ex) {
                throw new ParseException(ex.getMessage(), counter);
            }
            counter++;
        }
        bufferedReader.close();
        return operations;
    }
}
